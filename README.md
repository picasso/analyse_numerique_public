# MOOC : Analyse numérique pour ingénieurs (prof. Marco Picasso)

This folder contains Jupyter Notebooks for the exercices of the MOOC:

- Chapter01
    - Ex1.1 - Lagrange interpolation
- Chapter02
    - Ex2.1 - BDF2, convergence order
- Chapter03
    - Ex3.1 - Numerical integration
- Chapter04
    - Ex4.1 - $`LL^T`$ decomposition
    - Ex4.2 - $`LU`$ decomposition
- Chapter05
    - Ex5.1 - System of non linear equations
    - Quiz5.4 - Newton method
- Chapter06
    - Ex6.1 - System of differential equations
    - Quiz6.3 - Forward Euler
    - Quiz6.4 - Backward Euler
- Chapter07
    - Ex7.1 - A non-linear problem
- Chapter11
    - fd2d - Finite difference 2d Poisson problem
